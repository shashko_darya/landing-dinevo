var skills = new Swiper('.our-skills .swiper-container', {
    pagination: '.our-skills .pagination',
    slidesPerView: 8,
    slidesPerGroup: 2,
    paginationClickable: true,
    nextButton: '.our-skills .swiper-button-next',
    prevButton: '.our-skills .swiper-button-prev',
    breakpoints: {
        // when window width is <= 320px
        320: {
            slidesPerView: 3
        },
        // when window width is <= 480px
        768: {
            slidesPerView: 4
        },
// when window width is <= 640px
        1200: {
            slidesPerView: 6
        }
    }
});
$(".navbar-toggle").on('click', function() {
    $(".text-skill").toggle();
});
$(".swiper-button-prev").on("click", function() {
    $(this).addClass('swing-animation').one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd", function(){
        $(this).removeClass('swing-animation');
    });
});
$(".swiper-button-next").on("click", function() {
    $(this).addClass('swing-animation').one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd", function(){
        $(this).removeClass('swing-animation');
    });
});

var team = new Swiper('.team .swiper-container', {
    pagination: '.team .swiper-pagination',
    slidesPerView: 4,
    slidesPerGroup: 4,
    paginationClickable: true,
    setWrapperSize:true,
    spaceBetween: 0,
    autoResize: false,
    nextButton: '.team .swiper-button-next',
    prevButton: '.team .swiper-button-prev'
});


var portfolio = new Swiper('.portfolio .swiper-container', {
    pagination: '.portfolio .swiper-pagination',
    slidesPerView: 4,
    slidesPerGroup: 4,
    paginationClickable: true,
    setWrapperSize:true,
    spaceBetween: 0,
    autoResize: false,
    nextButton: '.portfolio .swiper-button-next',
    prevButton: '.portfolio .swiper-button-prev'
});


var reviwes = new Swiper('.reviwes .swiper-container', {
    pagination: '.reviwes .swiper-pagination',
    slidesPerView: 1,
    paginationClickable: true,
    nextButton: '.reviwes .swiper-button-next',
    prevButton: '.reviwes .swiper-button-prev'
});

/*$(".team .swiper-slide").click(function(){
    $(".modal").modal('show');
});*/
$('.tooltiper').tooltipster({
    content: $('<span><li style="list-style: disc;">PHP - 2 years experiance</li></span>'),
    position: 'bottom-left'
});